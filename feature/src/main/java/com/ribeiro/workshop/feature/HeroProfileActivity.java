package com.ribeiro.workshop.feature;

import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.ribeiro.workshop.feature.google.model.HeroViewModel;
import com.ribeiro.workshop.feature.mvp.model.entity.Hero;

public class HeroProfileActivity extends LifecycleActivity {

    public static final String EXTRA_KEY_UID = "uid";
    private HeroViewModel viewModel = null;

    public static Intent newIntent(Context context, String heroId) {
        Intent intent = new Intent(context, HeroProfileActivity.class);
        intent.putExtra(EXTRA_KEY_UID, heroId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gheroes);

        String userId = getIntent().getStringExtra(EXTRA_KEY_UID);
        viewModel = ViewModelProviders.of(this).get(HeroViewModel.class);
        viewModel.init(userId);

        viewModel.getHero().observe(this, new Observer<Hero>() {
            @Override
            public void onChanged(@Nullable Hero hero) {
                setupWidgets(hero);
            }
        });
    }

    private void setupWidgets(Hero hero) {
        if(hero == null) {
            return;
        }

        TextView nameTextView = findViewById(R.id.hero_txt_name);
        nameTextView.setText(hero.name);

        TextView groupTextView = findViewById(R.id.hero_txt_group);
        groupTextView.setText(hero.group);

        TextView powerTextView = findViewById(R.id.hero_txt_power);
        powerTextView.setText(String.valueOf(hero.powerLevel));
    }
}
