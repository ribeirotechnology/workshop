package com.ribeiro.workshop.feature;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ribeiro.workshop.feature.mvp.model.entity.Hero;
import com.ribeiro.workshop.feature.mvp.presenter.heroes.HeroesPresenter;
import com.ribeiro.workshop.feature.mvp.view.adapter.heroes.HeroesAdapter;
import com.ribeiro.workshop.feature.mvp.view.heroes.HeroesView;

import java.util.List;

public class HeroesActivity extends AppCompatActivity implements HeroesView {

    private HeroesPresenter presenter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heroes);

        getPresenter().fetchHeroes();
    }

    private HeroesPresenter getPresenter() {
        if(presenter == null) {
            presenter = new HeroesPresenter(this);
        }
        return presenter;
    }

    @Override
    public void showLoading() {
        //TODO add progress bar
    }

    @Override
    public void hideLoading() {
        //TODO hide progress bar
    }

    @Override
    public void showHeroes(List<Hero> bunchOfHeroes) {
        final ListView listView = findViewById(R.id.heroes_lst);
        listView.setAdapter(new HeroesAdapter(this, bunchOfHeroes));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                HeroesAdapter adapter = (HeroesAdapter) listView.getAdapter();
                String heroId = adapter.getItem(position).name;
                startActivity(HeroProfileActivity.newIntent(HeroesActivity.this, heroId));
            }
        });
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, "Algum erro aconteceu", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showInternetConnectionMessage() {
        Toast.makeText(this, "Por favor, verifique sua conexão com a internet",
                Toast.LENGTH_LONG).show();
    }
}
