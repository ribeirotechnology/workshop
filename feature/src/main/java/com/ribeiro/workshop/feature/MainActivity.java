package com.ribeiro.workshop.feature;

import android.content.Intent;
import android.os.Bundle;
import android.support.animation.DynamicAnimation;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.design.widget.NavigationView;
import android.support.text.emoji.EmojiCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import static android.support.animation.SpringForce.DAMPING_RATIO_HIGH_BOUNCY;
import static android.support.animation.SpringForce.STIFFNESS_VERY_LOW;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String EMOJI = "\uD83C\uDF2E";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.fab).setOnClickListener(getSpringAnimationListener());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setToolbar(toolbar);
        setupNavigationDrawer(toolbar);
    }

    private void setToolbar(Toolbar toolbar) {
        if(toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
    }

    private void setupNavigationDrawer(Toolbar toolbar) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if(toolbar == null || drawer == null) {
            return;
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_font) {
            startActivity(new Intent(this, MySupportActivity.class));
        } else if (id == R.id.nav_emoji) {
            setEmoji();
        } else if (id == R.id.nav_notification) {
            startActivity(new Intent(this, NotificationActivity.class));
        } else if (id == R.id.nav_mvp) {
            startActivity(new Intent(this, HeroesActivity.class));
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setEmoji() {
        final TextView textView = findViewById(R.id.main_txt);
        EmojiCompat.get().registerInitCallback(new EmojiCompat.InitCallback() {
            @Override
            public void onInitialized() {
                final EmojiCompat compat = EmojiCompat.get();
                textView.setText(
                        compat.process(EMOJI));
            }
        });

        //old way
//        textView.setText((EMOJI));
    }

    private View.OnClickListener getSpringAnimationListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpringForce force = new SpringForce();
                force.setStiffness(STIFFNESS_VERY_LOW);
                force.setDampingRatio(DAMPING_RATIO_HIGH_BOUNCY);

                SpringAnimation animation =
                        new SpringAnimation(view, DynamicAnimation.TRANSLATION_Y);
                animation.setStartVelocity(0.5F);
                animation.setSpring(force);
                animation.animateToFinalPosition(1000);
            }
        };
    }

    @Override public void onPointerCaptureChanged(boolean hasCapture) {}
}
