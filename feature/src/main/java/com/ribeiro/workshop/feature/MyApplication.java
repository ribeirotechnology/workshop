package com.ribeiro.workshop.feature;

import android.app.Application;
import android.content.Context;
import android.support.text.emoji.EmojiCompat;
import android.support.text.emoji.bundled.BundledEmojiCompatConfig;

import com.ribeiro.workshop.feature.notification.NotificationHelper;

import java.lang.ref.WeakReference;

/**
 * Created by luisribeiro on 15/06/17.
 */

public class MyApplication extends Application {

    private static WeakReference<Context> contextWeakReference;

    public static Context getAppContext() {
        try {
            return contextWeakReference.get();
        } catch (Throwable e) {
            return null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        contextWeakReference = new WeakReference<>(getApplicationContext());

        initEmoji();

        NotificationHelper.createChannels(this);
    }

    private void initEmoji() {
        EmojiCompat.Config config = new BundledEmojiCompatConfig(getApplicationContext());
        EmojiCompat.init(config);
    }
}
