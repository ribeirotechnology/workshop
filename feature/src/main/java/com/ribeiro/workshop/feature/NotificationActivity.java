package com.ribeiro.workshop.feature;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ribeiro.workshop.feature.notification.NotificationHelper;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {

    private int notifyID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        findViewById(R.id.notification_btn_promo).setOnClickListener(this);
        findViewById(R.id.notification_btn_message).setOnClickListener(this);
        findViewById(R.id.notification_btn_status).setOnClickListener(this);

        notifyID = 1;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.notification_btn_promo) {
            sendPromoNotification();
            return;
        }

        if(id == R.id.notification_btn_message) {
            sendMessageNotification();
            return;
        }

        if(id == R.id.notification_btn_status) {
            sendStatusNotification();
            return;
        }
    }

    private void sendPromoNotification() {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(this)
                .setContentTitle("Veja estes preços")
                .setContentText("Aproveite esta incrível oportunidade. Só hoje!!")
                .setSmallIcon(R.mipmap.ic_launcher);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = builder.setChannelId(NotificationHelper.Channel.PROMO.id);
        }

        mNotificationManager.notify(notifyID++, builder.build());
    }

    private void sendMessageNotification() {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(this)
                .setContentTitle("Você tem uma mensagem")
                .setContentText("Hey, cara! Passando para perguntar se vai ter Coup.")
                .setSmallIcon(R.mipmap.ic_launcher);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = builder.setChannelId(NotificationHelper.Channel.MESSAGE.id);
        }

        mNotificationManager.notify(notifyID++, builder.build());
    }

    private void sendStatusNotification() {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(this)
                .setContentTitle("Sua reserva foi confirmada")
                .setContentText("Sua reserva foi paga e já está confirmada. Obrigado por nos escolher!")
                .setSmallIcon(R.mipmap.ic_launcher);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = builder.setChannelId(NotificationHelper.Channel.STATUS.id);
        }

        mNotificationManager.notify(notifyID++, builder.build());
    }
}
