package com.ribeiro.workshop.feature.google.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.ribeiro.workshop.feature.mvp.model.entity.Hero;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by luisribeiro on 15/06/17.
 */

@Dao
public interface HeroDAO {
    @Insert(onConflict = REPLACE)
    void save(Hero hero);

    @Query("SELECT * FROM hero WHERE name = :heroId")
    LiveData<Hero> load(String heroId);
}
