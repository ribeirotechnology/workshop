package com.ribeiro.workshop.feature.google.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.ribeiro.workshop.feature.mvp.model.entity.Hero;

/**
 * Created by luisribeiro on 15/06/17.
 */

@Database(entities = {Hero.class}, version = 1)
public abstract class MyDatabase extends RoomDatabase {
    public static final String NAME = "SomeHeroes";

    public abstract HeroDAO heroDAO();
}
