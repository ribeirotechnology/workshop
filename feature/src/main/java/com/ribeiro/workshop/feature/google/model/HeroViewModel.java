package com.ribeiro.workshop.feature.google.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.ribeiro.workshop.feature.google.repository.HeroRepository;
import com.ribeiro.workshop.feature.mvp.model.entity.Hero;

import javax.inject.Inject;

/**
 * Created by luisribeiro on 15/06/17.
 */

public class HeroViewModel extends ViewModel {

    private HeroRepository heroRepository;
    private LiveData<Hero> hero;

    public HeroViewModel() {
        this.heroRepository = HeroRepository.getInstance();
    }

    @Inject
    public HeroViewModel(HeroRepository heroRepository) {
        this.heroRepository = heroRepository;
    }

    public void init(String heroId) {
        if(hero != null) {
            return;
        }
        this.hero = heroRepository.getHero(heroId);
    }

    public LiveData<Hero> getHero() {
        return hero;
    }
}
