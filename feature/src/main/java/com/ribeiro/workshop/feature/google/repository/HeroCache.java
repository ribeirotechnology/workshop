package com.ribeiro.workshop.feature.google.repository;

import android.arch.lifecycle.LiveData;

import com.ribeiro.workshop.feature.mvp.model.entity.Hero;

import java.util.HashMap;

/**
 * Created by luisribeiro on 15/06/17.
 */

class HeroCache extends HashMap<String, LiveData<Hero>> {

}
