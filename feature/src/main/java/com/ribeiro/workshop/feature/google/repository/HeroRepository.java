package com.ribeiro.workshop.feature.google.repository;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;

import com.ribeiro.workshop.feature.MyApplication;
import com.ribeiro.workshop.feature.google.dao.HeroDAO;
import com.ribeiro.workshop.feature.google.dao.MyDatabase;
import com.ribeiro.workshop.feature.mvp.model.dao.heroes.HeroesDAO;
import com.ribeiro.workshop.feature.mvp.model.entity.Hero;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by luisribeiro on 15/06/17.
 */

@Singleton
public class HeroRepository {

    private final static long FRESH_TIMEOUT = 300000L;

    private HeroDAO heroDAO;
    private Executor executor;
    private static HeroRepository instance = null;

    public static HeroRepository getInstance() {
        if(instance == null) {
            init();
        }
        return instance;
    }

    private static void init() {
        MyDatabase db = Room.databaseBuilder(MyApplication.getAppContext(),
                MyDatabase.class, MyDatabase.NAME).build();

        Executor executor = new Executor() {
            @Override
            public void execute(@NonNull Runnable runnable) {
                new Thread(runnable).start();
            }
        };

        instance = new HeroRepository(db.heroDAO(), executor);
    }

    @Inject
    public HeroRepository(HeroDAO heroDAO, Executor executor) {
        this.heroDAO = heroDAO;
        this.executor = executor;
    }

    public LiveData<Hero> getHero(String heroId) {
        LiveData<Hero> heroLiveData = heroDAO.load(heroId);
        if(shouldUpdate(heroLiveData)) {
            //it's in a different thread
            refreshHero(heroId);
        }

        //the data could be updated here
        return heroLiveData;
    }

    private boolean shouldUpdate(LiveData<Hero> heroLiveData) {
        if(heroLiveData == null || heroLiveData.getValue() == null) {
            return true;
        }

        long lastUpdate = heroLiveData.getValue().lastUpdate;
        return Math.abs(new Date().getTime() - lastUpdate) >= FRESH_TIMEOUT;
    }

    private void refreshHero(final String heroId) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                refreshHeroInTheSameThread(heroId);
            }
        });
    }

    private void refreshHeroInTheSameThread(String heroId) {
        //TODO - this part should be the external caller - something like retrofit
        List<Hero> bunchOfHeroes = new HeroesDAO().fetchHeroes();
        for (Hero hero: bunchOfHeroes) {
            if(hero.name.equals(heroId)) {
                hero.lastUpdate = new Date().getTime();
                heroDAO.save(hero);
                break;
            }
        }
    }
}
