package com.ribeiro.workshop.feature.mvp.model.business;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.ribeiro.workshop.feature.MyApplication;
import com.ribeiro.workshop.feature.mvp.model.exception.NoConnectionException;

/**
 * Created by luisribeiro on 15/06/17.
 */

public abstract class BaseBO {

    public void checkConnection() {
        Context context = MyApplication.getAppContext();

        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager != null) {
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            if(netInfo != null && netInfo.isConnectedOrConnecting()) {
                return;
            }
        }

        throw new NoConnectionException();
    }
}
