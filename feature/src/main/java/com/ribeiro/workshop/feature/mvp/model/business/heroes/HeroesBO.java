package com.ribeiro.workshop.feature.mvp.model.business.heroes;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.ribeiro.workshop.feature.MyApplication;
import com.ribeiro.workshop.feature.mvp.model.business.BaseBO;
import com.ribeiro.workshop.feature.mvp.model.dao.heroes.HeroesDAO;
import com.ribeiro.workshop.feature.mvp.model.entity.Hero;
import com.ribeiro.workshop.feature.mvp.model.exception.NoConnectionException;
import com.ribeiro.workshop.feature.mvp.model.exception.NoResultsException;

import java.util.List;

/**
 * Created by luisribeiro on 15/06/17.
 */

public class HeroesBO extends BaseBO {

    private final HeroesDAO heroesDAO;

    public HeroesBO() {
        this.heroesDAO = new HeroesDAO();
    }

    public List<Hero> fetchHeroes() {
        checkConnection();

        List<Hero> bunchOfHeroes = heroesDAO.fetchHeroes();
        if(bunchOfHeroes == null || bunchOfHeroes.isEmpty()) {
            throw new NoResultsException();
        }
        return bunchOfHeroes;
    }
}
