package com.ribeiro.workshop.feature.mvp.model.dao.heroes;

import android.os.SystemClock;

import com.ribeiro.workshop.feature.mvp.model.entity.Hero;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luisribeiro on 15/06/17.
 */

public class HeroesDAO {
    public List<Hero> fetchHeroes() {
        //TODO - this layer MUST to call an external API or something like that
        //an alternative is to call local database

        SystemClock.sleep(1000L);

        return buildFakeList();
    }

    private List<Hero> buildFakeList() {
        List<Hero> result = new ArrayList<>();

        result.add(new Hero("Goku", 9001, "DB"));//over nine thousand
        result.add(new Hero("Yusuke", 8000, "Yu Yu Hakusho"));
        result.add(new Hero("Batman", 7000, "DC"));
        result.add(new Hero("Vegeta", 5000, "DB"));
        result.add(new Hero("Hiei", 4000, "Yu yu Hakusho"));
        result.add(new Hero("Black Kamen Rider", 4000, "BKR"));
        result.add(new Hero("Ikki", 150, "CDZ"));
        result.add(new Hero("Ryu", 120, "Street Fighters"));
        result.add(new Hero("Ranger Vermelho", 110, "Power Rangers"));
        result.add(new Hero("Kulilin", 100, "DB"));
        result.add(new Hero("Harry Potter", 50, "HP"));
        result.add(new Hero("Jigglypuff", 11, "Pokemon"));
        result.add(new Hero("Seiya", 10, "CDZ"));
        result.add(new Hero("Gavião Arqueiro", 10, "Avangers"));
        result.add(new Hero("Videl", 5, "DB"));
        result.add(new Hero("Homem Codorna", 3, "Doug"));

        return result;
    }
}
