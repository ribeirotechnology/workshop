package com.ribeiro.workshop.feature.mvp.model.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by luisribeiro on 15/06/17.
 */

@Entity (tableName = "hero")
public class Hero {

    @PrimaryKey
    public int id;

    public String name;
    public long powerLevel;
    public String group;
    public long lastUpdate;

    public Hero() {}

    @Ignore
    public Hero(String name, long powerLevel, String group) {
        this.name = name;
        this.powerLevel = powerLevel;
        this.group = group;
    }
}
