package com.ribeiro.workshop.feature.mvp.presenter;

public class AsyncTaskResult<T> {

    private T result = null;

    private Throwable error = null;

    public AsyncTaskResult() {
    }

    public AsyncTaskResult(T result) {
        this.result = result;
    }

    public AsyncTaskResult(Throwable error) {
        this.error = error;
    }

    public AsyncTaskResult(T result, Throwable error) {
        this.result = result;
        this.error = error;
    }

    public T getResult() {
        return result;
    }

    public Throwable getError() {
        return error;
    }

    public boolean hasError() {
        return error != null;
    }
}
