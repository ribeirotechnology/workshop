package com.ribeiro.workshop.feature.mvp.presenter;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * The unique purpose to develop this class is to let the code cleaner on the presenter
 * CustomAsyncTask must be used only on Presenters
 */
public abstract class CustomAsyncTask<T> extends AsyncTask<Void, Void, AsyncTaskResult<T>> {

    @Override
    protected AsyncTaskResult<T> doInBackground(Void... params) {
        try {
            return runTask();
        } catch (Throwable e) {
            e.printStackTrace();
            Log.e(CustomAsyncTask.class.getSimpleName(), getMessage(e), e);
            return new AsyncTaskResult<>(e);
        }
    }

    @NonNull
    private String getMessage(Throwable e) {
        return "Error on AsyncTask - " + (e != null ? e.getLocalizedMessage() : "null object");
    }

    public void executeTask() {
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    protected abstract AsyncTaskResult<T> runTask();
}
