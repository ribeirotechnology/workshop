package com.ribeiro.workshop.feature.mvp.presenter.heroes;

import com.ribeiro.workshop.feature.mvp.model.business.heroes.HeroesBO;
import com.ribeiro.workshop.feature.mvp.model.entity.Hero;
import com.ribeiro.workshop.feature.mvp.model.exception.NoConnectionException;
import com.ribeiro.workshop.feature.mvp.presenter.AsyncTaskResult;
import com.ribeiro.workshop.feature.mvp.presenter.CustomAsyncTask;
import com.ribeiro.workshop.feature.mvp.view.heroes.HeroesView;

import java.util.List;

/**
 * Created by luisribeiro on 15/06/17.
 */

public class HeroesPresenter {

    private HeroesView view;
    private final HeroesBO heroesBO;

    public HeroesPresenter(HeroesView view) {
        this.view = view;
        this.heroesBO = new HeroesBO();
    }

    public void fetchHeroes() {
        new CustomAsyncTask<List<Hero>>(){
            @Override
            protected AsyncTaskResult<List<Hero>> runTask() {
                List<Hero> bunchOfHeroes = heroesBO.fetchHeroes();
                return new AsyncTaskResult<>(bunchOfHeroes);
            }

            @Override
            protected void onPostExecute(AsyncTaskResult<List<Hero>> result) {
                super.onPostExecute(result);

                if(result.hasError()) {
                    handleError(result.getError());
                    return;
                }

                view.showHeroes(result.getResult());
            }
        }.executeTask();
    }

    private void handleError(Throwable error) {
        if(error instanceof NoConnectionException) {
            view.showInternetConnectionMessage();
            return;
        }

        view.showErrorMessage();
    }
}
