package com.ribeiro.workshop.feature.mvp.view;

/**
 * Created by luisribeiro on 15/06/17.
 */

public interface BaseView {
    void showLoading();
    void hideLoading();

    void showInternetConnectionMessage();
}
