package com.ribeiro.workshop.feature.mvp.view.adapter.heroes;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ribeiro.workshop.feature.R;
import com.ribeiro.workshop.feature.mvp.model.entity.Hero;

import java.util.List;

/**
 * Created by luisribeiro on 15/06/17.
 */

public class HeroesAdapter extends BaseAdapter {
    private Context context;
    private List<Hero> bunchOfHeroes;

    public HeroesAdapter(Context context, List<Hero> bunchOfHeroes) {
        this.context = context;
        this.bunchOfHeroes = bunchOfHeroes;
    }

    @Override
    public int getCount() {
        return bunchOfHeroes == null ? 0 : bunchOfHeroes.size();
    }

    @Override
    public Hero getItem(int position) {
        return bunchOfHeroes == null ? null : bunchOfHeroes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if(convertView == null) {
            convertView = View.inflate(context, R.layout.item_hero, null);
        }

        Hero hero = getItem(position);

        TextView nameTextView = convertView.findViewById(R.id.hero_txt_name);
        nameTextView.setText(hero.name);

        TextView groupTextView = convertView.findViewById(R.id.hero_txt_group);
        groupTextView.setText(hero.group);

        TextView powerTextView = convertView.findViewById(R.id.hero_txt_power);
        powerTextView.setText(String.valueOf(hero.powerLevel));

        return convertView;
    }
}
