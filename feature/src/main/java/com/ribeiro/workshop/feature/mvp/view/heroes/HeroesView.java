package com.ribeiro.workshop.feature.mvp.view.heroes;

import com.ribeiro.workshop.feature.mvp.model.entity.Hero;
import com.ribeiro.workshop.feature.mvp.view.BaseView;

import java.util.List;

/**
 * Created by luisribeiro on 15/06/17.
 */

public interface HeroesView extends BaseView {

    void showHeroes(List<Hero> bunchOfHeroes);

    void showErrorMessage();
}
