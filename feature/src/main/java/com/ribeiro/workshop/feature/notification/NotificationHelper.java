package com.ribeiro.workshop.feature.notification;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;

/**
 * Created by luisribeiro on 15/06/17.
 */

public class NotificationHelper {

    @TargetApi(Build.VERSION_CODES.O)
    public static void createChannels(Context context) {
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Channel[] bunchOfChannelTypes = Channel.values();
        for (Channel channelType : bunchOfChannelTypes) {
            mNotificationManager.createNotificationChannel(channelType.build());
        }
    }

    public enum Channel {
        PROMO("my_channel_promo", "Promoções", NotificationManager.IMPORTANCE_LOW),
        STATUS("my_channel_status", "Status", NotificationManager.IMPORTANCE_HIGH),
        MESSAGE("my_channel_message", "Mensagens", NotificationManager.IMPORTANCE_MAX);

        public final String id;
        public final String name;
        public final int importance;
        public boolean enableLights = true;
        public int color = Color.RED;
        public boolean enableVibration = true;
        public long[] vibrationPattern = new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400};

        Channel(String id, String name, int importance) {
            this.id = id;
            this.name = name;
            this.importance = importance;
        }

        Channel(String id, String name, int importance, boolean enableLights, int color,
                boolean enableVibration, long[] vibrationPattern) {
            this.id = id;
            this.name = name;
            this.importance = importance;
            this.enableLights = enableLights;
            this.color = color;
            this.enableVibration = enableVibration;
            this.vibrationPattern = vibrationPattern;
        }

        @TargetApi(Build.VERSION_CODES.O)
        public NotificationChannel build() {
            NotificationChannel channel = new NotificationChannel(id, name, importance);
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(vibrationPattern);
            return channel;
        }
    }
}
