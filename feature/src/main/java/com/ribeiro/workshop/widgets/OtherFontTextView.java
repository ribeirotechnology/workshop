package com.ribeiro.workshop.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by luisribeiro on 15/06/17.
 */

public class OtherFontTextView extends AppCompatTextView {
    public OtherFontTextView(Context context) {
        super(context);
        setFont();
    }

    public OtherFontTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public OtherFontTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont();
    }

    private void setFont() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/ArtySignature.otf");
        setTypeface(typeface);
    }
}
